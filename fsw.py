def calendrier():
    date = str(input("Veuillez entrer une date suivant l'exemple suivant: 1er août 1947\n"))
    dateSaisie=str(date)
    valeurMois= {
            "janvier":0,"février":3,"mars":3,"avril":6,"mai":1,"juin":4,
            "juillet":6,"août":2,"septembre":5,"octobre":0,
            "novembre":3,"decembre":5
            }

    valeurSiecle= {16:6,17:4,18:2,19:0,20:6,21:4}

    valeurJour= {
            0:"Dimanche",1:"Lundi",2:"Mardi",
            3:"Mercredi",4:"Jeudi",5:"Vendredi",6:"Samedi"
            }

    
    #Récupération des informations
    date = date.split()
    annee = date[2]
    mois = date[1]
    if (date[0][0]=="1" and not date[0][1].isdigit()):      #Permet d'extraire l'entier dans le cas où l'utilisateur entre "1er"
        jour = int(date[0][0])
    else: jour=int(date[0])

    #Calculs
    deci = int(annee[2:])                                   #Récupère les deux derniers chiffres de l'année
    siecle = int(annee[:2])                                 # "          "     premiers     "
    jourS = deci // 4 + deci + jour + valeurMois.get(mois)  
    if int(annee) % 4 == 0 and (mois=="janvier"or mois=="février"):     #Vérification si année bissextile
        jourS -=1
    jourS += valeurSiecle.get(siecle)
    jourS %= 7
    print("Le "+dateSaisie+" sera un "+valeurJour.get(jourS)+".")       #Affichage du jour correspondant au reste

calendrier()
